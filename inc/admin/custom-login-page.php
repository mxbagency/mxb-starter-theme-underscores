<?php function my_login_logo() { ?>
    <style type="text/css">

        body.login {
            background: #283636 url(<?php echo get_stylesheet_directory_uri(); ?>/inc/admin/img/hero.png) center center/cover;
        }

        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/inc/admin/img/x.png);
    		height:80px;
    		width:100px;
    		background-size: contain;
    		background-repeat: no-repeat;
            padding-bottom: 10px;
            outline:0;
        }

        .aiowps-captcha-equation {
            color: #fff !important;
        }

        body.login div#login form#loginform {
            background:rgba(255,255,255,0.14);
            border-radius: 3px;
            padding: 26px 24px 35px 24px;
        }

        body.login div#login form#loginform p label {
            color:#fff;
            font-size: 15px;
        }

        body.login div#login #nav a, body.login div#login #backtoblog a{
            color: #05dda8;
            transition: all 250ms ease;
        }

        body.login div#login #nav a:hover, body.login div#login #backtoblog a:hover{
            color: #04ab82;
            transition: all 250ms ease;
        }

        body.login div#login form#loginform p.submit input#wp-submit{
            color: #fff;
            text-shadow: none;
            box-shadow: none;
            background: #05dda8;
            border-radius: 3px;
            font-size: 15px;
            font-weight: 600;
            height: auto;
            padding: 4px 20px;
            transition: all 250ms ease;
            outline:0;
            border:0;
        }

        body.login div#login form#loginform p.submit input#wp-submit:hover {
            background: #04ab82;
        }

        body.login #nav, body.login #backtoblog{
            text-align:center;
        }

    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
    return 'https://www.hellomxb.com';
}