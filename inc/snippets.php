<?php

// MXB CUSTOM LOGIN
function mxb_login() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/admin/mxb-login-style.css" />';
}
add_action('login_head', 'mxb_login');

// CUSTOM EXCERPT
function get_excerpt($count){
  $excerpt = get_the_content();
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = $excerpt.'...';
  return $excerpt;
}

//ACF OPTIONS PAGE
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

//ACF CUSTOM WYSIWYG TOOLBARS
add_filter( 'acf/fields/wysiwyg/toolbars' , 'custom_toolbars'  );
function custom_toolbars( $toolbars )
{
	// Uncomment to view format of $toolbars
	// echo '< pre >';
	// 	print_r($toolbars);
	// echo '< /pre >';
	// die;

	$toolbars['Very Simple' ] = array();
	$toolbars['Very Simple'][1] = array('bold' , 'italic' , 'underline', 'link' );
	$toolbars['Very Simple + Lists' ] = array();
	$toolbars['Very Simple + Lists'][1] = array('bold' , 'italic' , 'underline', 'link', 'bullist', 'numlist' );

	unset( $toolbars['Basic' ] );
	return $toolbars;
}