<?php
/**
 * Replace Me functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Replace_Me
 */

if ( ! function_exists( 'replace_me_setup' ) ) :
	function replace_me_setup() {

		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array(
			'primary-menu' => esc_html__( 'Primary', 'replace-me' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
add_action( 'after_setup_theme', 'replace_me_setup' );


function replace_me_scripts() {
	wp_enqueue_style( 'replace-me-style', get_stylesheet_uri() );
	wp_enqueue_style('slick-style', get_template_directory_uri() . '/inc/slick/slick.css', '', '', false);
	wp_enqueue_style('slick-style-theme', get_template_directory_uri() . '/inc/slick/slick-theme.css', '', '', false);

	wp_enqueue_script('main-js', get_template_directory_uri() . '/js/main.js', array('jquery', 'slick',), '', true);
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap/bootstrap.min.js', '', '', true);
	wp_enqueue_script('slick', get_template_directory_uri() . '/inc/slick/slick.min.js', '', '', true);
}
add_action( 'wp_enqueue_scripts', 'replace_me_scripts' );


if(file_exists(get_template_directory() . '/inc/template-tags.php')) {
    require get_template_directory() . '/inc/template-tags.php';
}

if(file_exists(get_template_directory() . '/inc/template-functions.php')) {
    require get_template_directory() . '/inc/template-functions.php';
}

if(file_exists(get_template_directory() . '/inc/post-types.php')) {
    require get_template_directory() . '/inc/post-types.php';
}

if(file_exists(get_template_directory() . '/inc/categories.php')) {
    require get_template_directory() . '/inc/categories.php';
}

if(file_exists(get_template_directory() . '/inc/snippets.php')) {
    require get_template_directory() . '/inc/snippets.php';
}