// // // CHANGE THIS // // //
var themeName = 'mxb';

// REQUIRE PLUGINS
var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');


//LOCAL DEVELOPMENT
gulp.task('theme-sass', function(done) {
  gulp.src('admin/mxb-login-style.scss')
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError)) 
  .pipe(autoprefixer())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('admin'));
  done();
});

gulp.task('watch',function(done) {
    gulp.watch('admin/*.scss',gulp.series('theme-sass'));
    done();
}); 